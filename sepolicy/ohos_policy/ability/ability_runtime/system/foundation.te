# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

allow foundation accessibility:binder { call };
allow foundation accesstoken_service:binder { call };
allow foundation accountmgr:binder { call };
allow foundation appspawn_socket:sock_file { write };
allow foundation appspawn:fd { use };
allow foundation appspawn:unix_stream_socket { connectto };
allow foundation bootevent_param:file { map open read };
allow foundation bootevent_param:parameter_service { set };
allow foundation bgtaskmgr_service:binder { call transfer };
allow foundation configfs:dir { remove_name rmdir search write };
allow foundation data_app_el1_file:file { getattr map read };
allow foundation data_file:dir { search };
allow foundation data_service_el1_file:dir { add_name create remove_name search write };
allow foundation data_service_el1_file:file { create ioctl unlink write open };
allow foundation data_service_file:dir { search };
allow foundation data_system_ce:dir { add_name search write };
allow foundation data_system_ce:file { create getattr ioctl lock map open read write };
allow foundation device_usage_stats_service:binder { call transfer };
allow foundation dev_mali:chr_file { ioctl };
allow foundation dev_unix_socket:dir { search };
allow foundation dev_unix_socket:sock_file { write };
allow foundation distributeddata:binder { call transfer };
allow foundation distributedfiledaemon:binder { call };
allow foundation distributedfileservice:binder { call };
allow foundation edm_sa:binder { call };
allow foundation foundation:unix_dgram_socket { getopt setopt };
allow foundation hdcd:binder { transfer };
allow foundation hdf_devmgr:binder { call transfer };
allow foundation hdf_allocator_service:hdf_devmgr_class { get };
allow foundation hiview:binder { transfer };
allow foundation memmgrservice:binder { call transfer };
allow foundation multimodalinput:binder { transfer };
allow foundation multimodalinput:unix_stream_socket { read };
allow foundation normal_hap_attr:process { sigkill signal };
allow foundation normal_hap_data_file_attr:file { read };
allow foundation persist_param:parameter_service { set };
allow foundation power_host:binder { call };
allow foundation render_service:binder { call transfer };
allow foundation render_service:fd { use };
allow foundation resource_schedule_service:binder { call transfer };
allow foundation sa_accesstoken_manager_service:samgr_class { get };
allow foundation sa_accountmgr:samgr_class { get };
allow foundation sa_bgtaskmgr:samgr_class { get };
allow foundation sa_device_service_manager:samgr_class { get };
allow foundation sa_distributeddata_service:samgr_class { get };
allow foundation sa_distributeschedule:samgr_class { get };
allow foundation sa_foundation_abilityms:samgr_class { add };
allow foundation sa_foundation_ans:samgr_class { add };
allow foundation sa_foundation_appms:samgr_class { add get };
allow foundation sa_foundation_battery_service:samgr_class { get };
allow foundation sa_foundation_bms:samgr_class { add };
allow foundation sa_foundation_devicemanager_service:samgr_class { add get };
allow foundation sa_foundation_tel_call_manager:samgr_class { add };
allow foundation sa_foundation_wms:samgr_class { get };
allow foundation sa_memory_manager_service:samgr_class { get };
allow foundation sa_msdp_devicestatus_service:samgr_class { get };
allow foundation sa_multimodalinput_service:samgr_class { get };
allow foundation sa_param_watcher:samgr_class { get };
allow foundation sa_softbus_service:samgr_class { get };
allow foundation sa_telephony_tel_cellular_call:samgr_class { get };
allow foundation screenlock_server:binder { call transfer };
allow foundation softbus_server:binder { call };
allow foundation sys_file:file { ioctl write };
allow foundation system_basic_hap_attr:binder { call transfer };
allow foundation system_basic_hap_attr:fd { use };
allow foundation system_basic_hap_attr:process { sigkill signal };
allow foundation system_basic_hap_data_file_attr:file { read };
allow foundation system_core_hap_attr:binder { call transfer };
allow foundation system_core_hap_attr:dir { search };
allow foundation system_core_hap_attr:file { getattr read };
allow foundation system_core_hap_attr:process { sigkill signal };
allow foundation system_core_hap_data_file_attr:file { read };
allow foundation system_lib_file:dir { getattr };
allow foundation vendor_etc_file:dir { search };
allow foundation work_scheduler_service:binder { call };
allow foundation quick_fix:binder { call transfer };
allowxperm foundation data_service_el1_file:file ioctl {  0x5413  };
allowxperm foundation data_system_ce:file ioctl {  0xf50c  };
allowxperm foundation dev_mali:chr_file ioctl {  0x8002  };
allowxperm foundation sys_file:file ioctl {  0x5413  };
allow foundation foundation:capability { sys_ptrace };
allow foundation storage_manager:dir { search };
allow foundation storage_manager:file { open read write getattr };
allow foundation sa_storage_manager_service:samgr_class { get };
allow foundation netmanager:binder { transfer };
allow foundation faultloggerd:fifo_file { read };
allow foundation exfat:file { read write };
allow foundation vfat:file { read write };
allow foundation ntfs:file { read write };
neverallow foundation *:process ptrace;

# add for hiperf
allow hiperf multimodalinput:fd { use };

# add for aa
developer_only(`
    allow aa sa_foundation_appms:samgr_class { get };
    allow aa samgr:binder { call };
    allow aa debug_param:file { map read open };
    allow aa hilog_param:file { map read open };
    allow aa sa_foundation_bms:samgr_class { get };
    allow aa foundation:binder { call transfer };
    allow aa foundation:fd { use };
    allow aa data_service_el1_file:file { read write };
    allow aa dev_console_file:chr_file { read write };
    allow aa hdcd:fd { use };
    allow aa hdcd:unix_stream_socket { read write };
    allow aa sh:fd { use };
    allow aa tty_device:chr_file { read write open ioctl };
    allow aa dev_unix_socket:dir { search };
    allow aa hdcd:fifo_file { ioctl read write };
    allow aa persist_sys_param:file { map open read };
    allow aa devpts:chr_file { ioctl read write };
    allow aa data_file:dir { search getattr};
    allow aa sa_foundation_abilityms:samgr_class { get };
    allow aa tracefs:dir { search };
    allow aa system_bin_file:dir { search };
    allow aa system_bin_file:file { getattr execute read open execute_no_trans map };
    allow aa system_bin_file:lnk_file { read };
    allow aa data_local:dir { search };
    allow aa aa_exec:file { execute_no_trans };
    allow aa hilog_output_socket:sock_file { write };
    allow aa hilog_control_socket:sock_file { write };
    allow aa hilogd:unix_stream_socket { connectto };
    allow aa sa_accountmgr:samgr_class { get };
    allow aa sa_foundation_bms:samgr_class { get };
    allow aa hilog_exec:file { getattr execute execute_no_trans map read open };
    allow aa init:dir { getattr search };
    allow aa init:file { open read };
    allow aa kernel:dir { getattr search };
    allow aa kernel:file { open read };
    allow aa watchdog_service:dir { getattr search };
    allow aa sh:fifo_file { ioctl write };
    allow aa arkcompiler_param:file { map open read };
    allow aa accessibility:binder { call transfer };
    allow aa normal_hap_attr:binder { call transfer };
    allow aa param_watcher:binder { call transfer };
    allow aa uitest_exec:file { execute getattr map read open };
    allow aa dev_ashmem_file:chr_file { open };
    allow aa multimodalinput:binder { call };
    allow aa hap_domain:fd { use };
    allow aa hap_file_attr:file { getattr ioctl read write };
    allow aa render_service:fd { use };
    allow aa sa_accessibleabilityms:samgr_class { get };
    allow aa sa_foundation_cesfwk_service:samgr_class { get };
    allow aa sa_foundation_dms:samgr_class { get };
    allow aa sa_multimodalinput_service:samgr_class { get };
    allow aa sa_param_watcher:samgr_class { get };
    allowxperm aa hap_file_attr:file ioctl { 0x5413 };
    allowxperm aa sh:fifo_file ioctl { 0x5413 };
    allowxperm aa devpts:chr_file ioctl { 0x5413 };
    allowxperm aa tty_device:chr_file ioctl { 0x5413 };
    allowxperm aa hdcd:fifo_file ioctl { 0x5413 };
    allow foundation aa:binder { call };
    allow samgr aa:dir { search };
    allow samgr aa:file { open read };
    allow samgr aa:process { getattr };
    allow samgr aa:binder { call transfer };
    allow hap_domain aa:binder { call };
    allow hiview aa:dir { search };
    allow hiview aa:file { read open getattr };
    allow hdcd aa:process { signal };
    allow param_watcher aa:binder { call };
    allow accessibility aa:binder { call transfer };
    allow normal_hap_attr aa:binder { transfer };
    allow hidumper_service aa:dir { search };
    allow hidumper_service aa:file { getattr open read };

    # avc:  denied  { getattr } for  pid=4155 comm="sh" path="/data/local/tmp" dev="mmcblk0p14" ino=106 scontext=u:r:aa:s0 tcontext=u:object_r:data_local_tmp:s0 tclass=dir permissive=1
    # avc:  denied  { write search } for  pid=4155 comm="sh" name="tmp" dev="mmcblk0p14" ino=106 scontext=u:r:aa:s0 tcontext=u:object_r:data_local_tmp:s0 tclass=dir permissive=1
    allow aa data_local_tmp:dir { getattr write search };

    # avc:  denied  { execute } for  pid=4176 comm="EventRunner#1" name="sh" dev="mmcblk0p7" ino=381 scontext=u:r:aa:s0 tcontext=u:object_r:sh_exec:s0 tclass=file permissive=1
    # avc:  denied  { execute_no_trans } for  pid=4176 comm="EventRunner#1" path="/system/bin/sh" dev="mmcblk0p7" ino=381 scontext=u:r:aa:s0 tcontext=u:object_r:sh_exec:s0 tclass=file permissive=1
    # avc:  denied  { map } for  pid=4176 comm="sh" path="/system/bin/sh" dev="mmcblk0p7" ino=381 scontext=u:r:aa:s0 tcontext=u:object_r:sh_exec:s0 tclass=file permissive=1
    # avc:  denied  { read open } for  pid=4176 comm="EventRunner#1" path="/system/bin/sh" dev="mmcblk0p7" ino=381 scontext=u:r:aa:s0 tcontext=u:object_r:sh_exec:s0 tclass=file permissive=1
    allow aa sh_exec:file { execute execute_no_trans map read open };
')
